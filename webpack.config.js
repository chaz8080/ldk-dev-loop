const path = require("path");
const merge = require("webpack-merge");
const ldkConfig = require("@oliveai/ldk/dist/webpack/config");
require("dotenv").config();
const Dotenv = require("dotenv-webpack");

const config = {
  entry: [path.resolve(__dirname, "./src/index.ts")],
  plugins: [new Dotenv()],
};

// .env should include a LOCAL_LOOP_PATH for hot reloading on restart
if (process.env.LOCAL_LOOP_PATH) {
  config.output = {
    path: process.env.LOCAL_LOOP_PATH,
  };
}

const merged = merge.merge(ldkConfig.default, config);

module.exports = merged;
