# ldk-dev-loop

Unofficial development loop for the loop development kit provided by Olive Helps.

## Loop Development Kit

To get started with Olive Helps and the LDK, follow the instructions [here](https://oliveai.dev/guides/getting-started).

To check out the LDK (loop development kit) source, click [here](https://github.com/open-olive/loop-development-kit).

## Motivation

It's nice to have a separate loop with helper functions to speed up development. For instance, if you're working on backend code and need an Olive Helps token to test, you can simply search `jwt` in the searchbar and the development loop will whisper it to you.

## Building

From `/ldk-dev-loop`

- `npm i`
- `npm run build`

This will generate a `./dist` directory from your `index.js` that contains your transpiled Loop.

You can now load this Loop into Olive Helps.

## Services and configuration

### init

No configuration necessary.

Anything you need to run on every loop start/restart, put in the init service handler

### jwt

No configuration necessary.

Just search for `jwt` either from the searchbar in Olive Helps, or the global search. You'll get a whisper with a new token. For convenience, the token is automagically written to your clipboard.

### websocket

Set a `WEBSOCKET_URL` and, if applicable `WEBSOCKET_AUTHORIZATION`, in your `.env` file (e.g.):

```.env
WEBSOCKET_URL=<wss://your.domain.com>
WEBSOCKET_AUTHORIZATION="Bearer eyesjklahsdfiuhalskjkhlasdf...."
```

Update the `package.json` with the appropriate values in the section `ldk -> permissions -> network -> urlDomains`.

<!-- TODO: add a component to send payloads over websocket on demand -->

## Hot Reloading-ish

In order to have hot reloading while developing on a loop, add a `.env` file to the root of the project and add the following:

```.env
LOCAL_LOOP_PATH=<your loop path>
```

The `LOCAL_LOOP_PATH` can be found by using the Olive Helps logs when your loop is installed. Example paths would be:

- **Windows (WSL):** `/mnt/c/Users/johnDoe/AppData/Roaming/Olive Helps/secureloops/033afdb3-4015-4808-a3b4-00a9465dc577_71665de6-9b96-4e43-b9f0-03b4818a9ba6_1628936198_local`

- **OSX:** `/Users/johnDoe/Library/Application Support/Olive Helps/secureloops/033afdb3-4015-4808-a3b4-00a9465dc577_71665de6-9b96-4e43-b9f0-03b4818a9ba6_1628936198_local`

Note that relative paths and aliases are not supported!

Now you can run `npm run dev`. Webpack will monitor you files, rebuild if it detects changes, output the new bundle to `LOCAL_LOOP_PATH`, and you can click the `Restart` button within the Local Loops section of Olive Helps' Loop Library.
