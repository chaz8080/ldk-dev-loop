/**
 * Allow observers to listen to searchbar search entries
 */
import { CustomUI, Search, SearchEvents } from "../../custom";

export const searchbar: CustomUI = Search();

searchbar.listenSearchbar((value: string) => {
  searchbar.eventEmitter.emit(SearchEvents.SEARCH, value);
});
