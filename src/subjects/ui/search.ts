/**
 * Allow observers to listen to ALL search events whether they be a global entry or searchbar entry
 */
import { CustomUI, Search, SearchEvents } from "../../custom";
import { globalSearch, searchbar } from "../ui";

export const search: CustomUI = Search();

const bubbleEvents = (value: string) => {
  search.eventEmitter.emit(SearchEvents.SEARCH, value);
};

globalSearch.eventEmitter.on(SearchEvents.SEARCH, bubbleEvents);
searchbar.eventEmitter.on(SearchEvents.SEARCH, bubbleEvents);
