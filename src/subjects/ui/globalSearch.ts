/**
 * Allow observers to listen to global search entries
 */
import { CustomUI, Search, SearchEvents } from "../../custom";

export const globalSearch: CustomUI = Search();

globalSearch.listenGlobalSearch((value: string) => {
  globalSearch.eventEmitter.emit(SearchEvents.SEARCH, value);
});
