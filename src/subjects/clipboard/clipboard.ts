/**
 * Allow observers to listen to clipboard events
 */
import { CustomClipboard, Clipboard, ClipboardEvents } from "../../custom";

export const clipboard: CustomClipboard = Clipboard();

clipboard.listen(false /** include Olive Helps Events */, (value: string) => {
  clipboard.eventEmitter.emit(ClipboardEvents.COPY, value);
});

clipboard.listen(true /** include Olive Helps Events */, (value: string) => {
  clipboard.eventEmitter.emit(ClipboardEvents.COPY_ALL, value);
});
