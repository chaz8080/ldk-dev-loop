import { websocketHelper } from "../../helpers";
import { CustomSocket } from "../../custom";

let socket: Promise<CustomSocket | null>;

(async () => {
  socket = websocketHelper.setupWebsocket();
})();

export { socket };
