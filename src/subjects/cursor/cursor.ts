/**
 * Allow observers to listen to cursor events
 */
import { Position } from "@oliveai/ldk/dist/cursor";
import { CustomCursor, Cursor, CursorEvents } from "../../custom";

export const cursor: CustomCursor = Cursor();

cursor.listenPosition((position: Position) => {
  cursor.eventEmitter.emit(CursorEvents.POSITION, position);
});
