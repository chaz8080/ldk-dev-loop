import { network } from "@oliveai/ldk";
import { SocketConfiguration } from "@oliveai/ldk/dist/network";
import { CustomSocket } from "custom/Socket";
import { basicWhisperMessage } from "../../whispers";
import { Socket, SocketEvents, SocketState } from "../../custom";

const getConfig = (): network.SocketConfiguration | null => {
  if (!process.env.WEBSOCKET_URL) {
    return null;
  }

  const config: SocketConfiguration = {
    url: `${process.env.WEBSOCKET_URL}`,
  };

  if (process.env.WEBSOCKET_AUTHORIZATION) {
    config.headers = {
      Authorization: [`${process.env.WEBSOCKET_AUTHORIZATION}`],
    };
  }

  return config;
};

const setupWebsocket = async (): Promise<CustomSocket | null> => {
  const config: network.SocketConfiguration | null = getConfig();

  if (!config) {
    basicWhisperMessage({
      label: "HELPER WEBSOCKET",
      body: "WebSocket config not found in .env",
    });
    return null;
  }

  const ws: CustomSocket | null = await Socket(config);
  if (!ws) {
    basicWhisperMessage({
      label: "HELPER WEBSOCKET",
      body: "WebSocket - something went wrong",
    });
    return null;
  }

  basicWhisperMessage({
    label: "HELPER WEBSOCKET",
    body: "WebSocket connected!!!",
  });

  ws.setCloseHandler((error, code, text) => {
    if (error) {
      ws.eventEmitter.emit(
        SocketEvents.ERROR,
        `Error: ${error.message}. Name: ${error.name}`
      );
      return;
    }

    ws.state = SocketState.CLOSED;
  });

  ws.setMessageHandler(
    (error: Error | undefined, message: string | Uint8Array) => {
      if (error) {
        ws.eventEmitter.emit(SocketEvents.ERROR, error);
        return;
      }

      ws.eventEmitter.emit(SocketEvents.MESSAGE, message);
    }
  );

  return ws;
};

export const websocketHelper = {
  setupWebsocket,
};
