import { clipboard } from "@oliveai/ldk";
import { Clipboard as OHClipboard } from "@oliveai/ldk/dist/clipboard";
import { CustomEventEmitter } from "./CustomEventEmitter";

export enum ClipboardEvents {
  COPY = "COPY",
  COPY_ALL = "COPY_ALL",
  READ = "READ",
  WRITE = "WRITE",
}

export interface CustomClipboard extends OHClipboard {
  readAndEmit(): Promise<string>;
  writeAndEmit(value: string): Promise<void>;
  eventEmitter: CustomEventEmitter;
}

export const Clipboard = (): CustomClipboard => {
  const eventEmitter = new CustomEventEmitter();

  const readAndEmit = () =>
    clipboard.read().then((value: string): Promise<string> => {
      eventEmitter.emit(ClipboardEvents.READ, value);
      return Promise.resolve(value);
    });

  const writeAndEmit = (value: string) =>
    clipboard.write(value).then((): void => {
      eventEmitter.emit(ClipboardEvents.WRITE, value);
    });

  return {
    ...clipboard,
    readAndEmit,
    writeAndEmit,
    eventEmitter,
  };
};
