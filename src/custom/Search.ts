import { ui } from "@oliveai/ldk";
import { UI } from "@oliveai/ldk/dist/ui";
import { CustomEventEmitter } from "./CustomEventEmitter";

export enum SearchEvents {
  SEARCH = "SEARCH",
}

export interface CustomUI extends UI {
  eventEmitter: CustomEventEmitter;
}

export const Search = (): CustomUI => ({
  ...ui,
  eventEmitter: new CustomEventEmitter(),
});
