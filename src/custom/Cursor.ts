import { cursor } from "@oliveai/ldk";
import { Cursor as OHCursor, Position } from "@oliveai/ldk/dist/cursor";
import { CustomEventEmitter } from "./CustomEventEmitter";

export enum CursorEvents {
  POSITION = "POSITION",
}

export interface CustomCursor extends OHCursor {
  positionAndEmit(): Promise<Position>;
  eventEmitter: CustomEventEmitter;
}

export const Cursor = (): CustomCursor => {
  const eventEmitter = new CustomEventEmitter();

  const positionAndEmit = () =>
    cursor.position().then((position) => {
      eventEmitter.emit(CursorEvents.POSITION, position);
      return position;
    });

  return { ...cursor, positionAndEmit, eventEmitter };
};
