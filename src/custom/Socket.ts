import { network } from "@oliveai/ldk";
import { CustomEventEmitter } from "./CustomEventEmitter";

export enum SocketState {
  CLOSED = "CLOSED",
  OPEN = "OPEN",
}

export enum SocketEvents {
  ERROR = "ERROR",
  MESSAGE = "MESSAGE",
}

export interface CustomSocket extends network.Socket {
  state: SocketState;
  eventEmitter: CustomEventEmitter;
}

export const Socket = async (
  config: network.SocketConfiguration
): Promise<CustomSocket | null> => {
  console.log(
    `CONNECTING TO WEBSOCKET WITH CONFIG: >>>> ${JSON.stringify(config)}`
  );

  return await network
    .webSocketConnect(config)
    .then((socket) => ({
      ...socket,
      state: SocketState.OPEN,
      eventEmitter: new CustomEventEmitter(),
    }))
    .catch((err) => {
      console.error(`couldn't get WebSocket connection - ${err}`);
      return null;
    });
};
