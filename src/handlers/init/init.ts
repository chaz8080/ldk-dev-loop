import { basicWhisperMessage } from "../../whispers";
import { Handler } from "../../handlers";

const handler = async () => {
  const date = new Date();
  basicWhisperMessage({
    label: "INIT",
    body: `LOOP STARTED @ ${date.toLocaleTimeString()}`,
  });
};

export const initHandler: Handler = {
  handle: handler,
};
