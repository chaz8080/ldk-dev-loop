import { basicWhisperMessage } from "../../whispers";
import { Handler } from "../../handlers";
import { socket } from "../../subjects";
import { CustomSocket, SocketState } from "../../custom";

const handle = (socket: CustomSocket | null) => {
  if (!socket) {
    basicWhisperMessage({
      label: "WEBSOCKET",
      body: "Socket service could not be started.",
    });
    return;
  }

  if (socket.state === SocketState.OPEN) {
    // TODO: Render a component that can send test payload messages over WebSocket
    // socket.writeMessage("TODO")
    return;
  }
};

const handler = async () => {
  await socket.then((socket) => handle(socket));
};

export const websocketHandler: Handler = {
  handle: handler,
};
