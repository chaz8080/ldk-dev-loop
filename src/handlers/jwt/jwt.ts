import { user } from "@oliveai/ldk";
import { refreshWhisperMessage } from "../../whispers";
import { Handler } from "../../handlers";

const handler = async () => {
  const jwt = await user.jwt();
  refreshWhisperMessage({
    label: "JWT",
    body: jwt,
    refreshContent: () => user.jwt(),
  });
};

export const jwtHandler: Handler = {
  handle: handler,
};
