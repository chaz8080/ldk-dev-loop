import { initService, jwtService, websocketService } from "./services";

initService.start();
jwtService.start();
websocketService.start();
