import { websocketHandler } from "../../handlers";
import { Service } from "../../services";

const start = async () => {
  websocketHandler.handle();
};

export const websocketService: Service = {
  start,
};
