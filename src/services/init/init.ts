import { Service } from "../service";
import { initHandler } from "../../handlers";

const start = () => {
  initHandler.handle();
};

export const initService: Service = {
  start,
};
