import { SearchEvents } from "../../custom";
import { jwtHandler } from "../../handlers";
import { search } from "../../subjects";
import { Service } from "../service";

const start = () => {
  search.eventEmitter.on(SearchEvents.SEARCH, (value: string) => {
    if (value !== "jwt") return;
    jwtHandler.handle();
  });
};

export const jwtService: Service = {
  start,
};
