import { whisper } from "@oliveai/ldk";
import { WhisperComponentType } from "@oliveai/ldk/dist/whisper";

interface BasicWhisperMessage {
  label: string | undefined;
  body: string | undefined;
  header?: string | undefined;
  id?: string | undefined;
  key?: string | undefined;
  style?: whisper.Urgency | undefined;
  textAlign?: whisper.TextAlign | undefined;
  tooltip?: string | undefined;
  onClose?: () => void;
}

export const basicWhisperMessage = async ({
  label,
  body,
  header,
  id,
  key,
  style,
  textAlign,
  tooltip,
  onClose = () => null,
}: BasicWhisperMessage): Promise<whisper.Whisper> =>
  whisper.create({
    label: `DEV$ ${label}`,
    onClose,
    components: [
      {
        type: WhisperComponentType.Message,
        body,
        header,
        id,
        key,
        style,
        textAlign,
        tooltip,
      },
    ],
  });
