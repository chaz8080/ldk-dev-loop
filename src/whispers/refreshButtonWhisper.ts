import { clipboard, whisper } from "@oliveai/ldk";
import {
  Button,
  Message,
  WhisperComponentType,
} from "@oliveai/ldk/dist/whisper";

interface BasicWhisperMessage {
  label: string | undefined;
  body: string;
  header?: string | undefined;
  id?: string | undefined;
  key?: string | undefined;
  style?: whisper.Urgency | undefined;
  textAlign?: whisper.TextAlign | undefined;
  tooltip?: string | undefined;
  onClose?: () => void;
  refreshContent: () => Promise<string>;
}

export const refreshWhisperMessage = async ({
  label,
  body,
  header,
  id,
  key,
  style,
  textAlign,
  tooltip,
  onClose = () => null,
  refreshContent,
}: BasicWhisperMessage): Promise<whisper.Whisper> => {
  await clipboard.write(body);

  let jwtComponent: Message = {
    type: WhisperComponentType.Message,
    body,
    header,
    id,
    key,
    style,
    textAlign,
    tooltip,
  };

  const refreshButton: Button = {
    type: WhisperComponentType.Button,
    buttonStyle: whisper.ButtonStyle.Primary,
    label: "REFRESH",
    onClick: async () => {
      const body = await refreshContent();
      await clipboard.write(body);

      jwtComponent = {
        ...jwtComponent,
        body,
      };

      jwtWhisp.update({
        components: [jwtComponent, refreshButton],
      });
    },
  };

  const jwtWhisp = await whisper.create({
    label: `DEV$ ${label}`,
    onClose,
    components: [jwtComponent, refreshButton],
  });

  return jwtWhisp;
};
